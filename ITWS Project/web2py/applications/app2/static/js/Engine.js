var states = {
	INIT: "INIT",
	READY: "READY",
	PLAYING: "PLAYING",
	WON: "WON",
	GAMEOVER: "GAMEOVER",
	PAUSED: "PAUSED"
};

var canvas = null;
var context = null;
var blitz = null;
var level = {};
var hero = {};
var badGuys = [];
var myAnimation = null;
var boundsData = null; // image data for the boundary map
var images = {}; // hash of images
var leftKeyup = true;
var rightKeyup = true;
var state = states.INIT;
var mytimer = null;
var Constants = {};
Constants.gravity = 2;
Constants.attackRange = 100; 
Constants.attackInterval = 30; 

this.bga = new Audio("/app2/static/sounds/bg.ogg");
this.bga.loop = true;
this.bga.load();
this.wa = new Audio("/app2/static/sounds/win.ogg");
this.goa = new Audio("/app2/static/sounds/lose.ogg");
this.ja = new Audio("/app2/static/sounds/jump.ogg");
this.ja.load();
this.punch = new Audio("/app2/static/sounds/punch.ogg");
this.punch.load();

// ========================================= INITIALIZE =========================================

 function init (){
    canvas = document.getElementById("myCanvas");
    context = canvas.getContext("2d");
    
    addKeyboardListeners();
    
    var sources = {                                 // Courtsey http://www.spriters-resource.com
        levelBounds: "/app2/static/images/level_bounds.png",
        level: "/app2/static/images/level.png",
        heroSprites: "/app2/static/images/hero_sprites.png",
	badGuySprites: "/app2/static/images/bad_guy_sprites.png",
        background: "/app2/static/images/ss.jpg",
        readyScreen: "/app2/static/images/123456.jpg",
        gameoverScreen: "/app2/static/images/gover.jpg",
        winScreen: "/app2/static/images/win.jpg"
    };
    
    loadImages(sources, function(){
        initGame();
    });
}

function loadImages(sources, callback){
    var loadedImages = 0;
    var numImages = 8;
    for (var src in sources) {
        images[src] = new Image();
        images[src].onload = function(){   
            if (++loadedImages >= numImages) {
                callback();
            }
        };
        images[src].src = sources[src];
    }
}

function setBoundsData(){
    canvas.width = 6944;
    context.drawImage(images.levelBounds, 0, 0);
   var imageData = context.getImageData(0, 0, 6944, 600);  
    boundsData = imageData.data;
    canvas.width = 900;
}

function initGame(){
    initLevel();
    initHero();
    initBadGuys();
    initHealthBar();
    inittimer();
    
    // instantiate new animation object
    myAnimation = new Animation(canvas);
    
    // set updateStage method
    myAnimation.setUpdateStage(function(){
        updateStage();
    });
    
    // set drawStage method
    myAnimation.setDrawStage(function(){
        drawStage();
    });
    
    setBoundsData();
        
    // game is now ready to play
    state = states.READY;
    drawScreen(images.readyScreen);
}

function initHealthBar(){
    healthBar = new HealthBar({
        maxHealth: hero.maxHealth,
        x: 10,
        y: 10,
        maxWidth: 150,
        height: 20
    });
}

function inittimer(){
            mytimer = new timer({
               x: 10,
               y: 10,
               maxWidth: 50,
               height: 20,
               t: 0,
               canvas : document.getElementById('myCanvas')
               });
}

function initLevel(){
    level = new Level({
        x: 0,
        y: 0,
        leftBounds: 100,
        rightBounds: 500
    });
}

function initHero(){
    // initialize Hero
    var heroMotions = {
        STANDING: {
            index: 0,
            numSprites: 5,
            loop: true
        },
        AIRBORNE: {
            index: 1,
            numSprites: 5,
            loop: false
        },
        RUNNING: {
            index: 2,
            numSprites: 6,
            loop: true
        },
	ATTACKING: {
            index: 3,
            numSprites: 5,
            loop: false
        }
    };
    
    hero = new Player({
        normalSpriteSheet: images.heroSprites, 
        x: 0,
        y: 381,
        playerSpeed: 300,
        motions: heroMotions,
        startMotion: heroMotions.STANDING,
        facingRight: true,
        moving: false,
        frameRate: 3,
        posRelativeToLevel: false,
        maxHealth: 5
    });

}

function initBadGuys(){
// initalize bad guys
// notice that AIRBORNE and RUNNING both use the same sprite animation

    var badGuyMotions = {
        ATTACKING: {
            index: 1,
            numSprites: 4,
            loop: false
        },
        AIRBORNE: {
            index:0 ,
            numSprites: 4,
            loop: false
        },
        RUNNING: {
            index:0,
            numSprites: 6,
            loop: true
        },
    };

var badGuyStartX = [600,2602,6402];

for (var n = 0; n < badGuyStartX.length; n++) {
        badGuys.push(new Player({
            normalSpriteSheet: images.badGuySprites,
            x: badGuyStartX[n],
            y: 381,
            playerSpeed: 100,
            motions: badGuyMotions,
            startMotion: badGuyMotions.RUNNING,
            facingRight: true,
            moving: true,
            frameRate: 3,
            posRelativeToLevel: true,
            maxHealth: 2
        }));
    }
}


// ========================================= GAME ENGINE =========================================
function addKeyboardListeners(){
    document.onkeydown = function(evt){
        handleKeydown(evt);
    };
    document.onkeyup = function(evt){
        handleKeyup(evt);
    };
}
   
function handleKeyup(evt){
   var keycode = ((evt.which) || (evt.keyCode));
    
        if (keycode==37) {      //Left
            leftKeyup = true;
            if (leftKeyup && rightKeyup) {
                hero.stop();
            }
        }
        
        else if (keycode==39) {      //Right
             rightKeyup = true;
            if (leftKeyup && rightKeyup) {
                hero.stop();
            } 
        }   
}

function handleKeydown(evt){
  var  keycode = ((evt.which) || (evt.keyCode));
     
        if(keycode == 13){               // enter
            if (state == states.READY) {
                state = states.PLAYING;
               
                this.bga.play();
                myAnimation.start();       // start animation
                }
            else if (state == states.GAMEOVER) {                
                resetGame();
                myAnimation.start();
                state = states.PLAYING;
            }
		else if(state == states.WON) {
		var time = Math.round(myAnimation.getTime());
                    time = Math.round(1000000*hero.health*3/(time)); 

		window.location.replace('/app2/default/create.html?time='+time);
	    }

        }
        else if(keycode== 37){            // left 
            leftKeyup = false;
            hero.moveLeft();
        }
            
        else if(keycode==38 && state == states.PLAYING) {
                this.ja.play();   // up
            hero.jump();
        }
    
        else if(keycode==39){             // right
            rightKeyup = false;
            hero.moveRight();
        }
        
        else if (keycode==80) {         // paused
            if (state == states.PLAYING) {
		    myAnimation.stop();
		    var toast = document.getElementById('pausedToast');
		    toast.style.visibility = 'visible'; 
		    state = states.PAUSED;
                    this.bga.pause();
    	    }
	    else if (state == states.PAUSED ) {
			var toast = document.getElementById('pausedToast');
			toast.style.visibility = 'hidden';
			myAnimation.start();
			state = states.PLAYING;
                        this.bga.play();
	    }
        }

	else if (keycode ==83) { // S for attack  
	        
            hero.attack();
            setTimeout(function(){
            this.punch.load();
            this.punch.play();                
                for (var n = 0; n < badGuys.length; n++) {
                    (function(){
                        var thisBadGuy = badGuys[n];
                        if (hero.nearby(thisBadGuy)) {
                            thisBadGuy.damage();
                        }
                    })();
                }
            }, Constants.attackInterval);
}
}

function drawStage(){
    if (state == states.PLAYING || state == states.GAMEOVER || state == states.WON) {
        level.draw();
	for (var n = 0; n < badGuys.length; n++) {
            var thisBadGuy = badGuys[n];
            thisBadGuy.draw();
        }
	healthBar.draw();
        hero.draw();
	
        mytimer.draw(Math.round(myAnimation.t));
        
        if (state == states.GAMEOVER) {
            myAnimation.stop();
            drawScreen(images.gameoverScreen);
        }
        else if (state == states.WON) {
            myAnimation.stop();
            drawScreen(images.winScreen);
        }
    }
    else if (state == states.READY) {
        drawScreen(images.readyScreen);
    }
}

function updateStage(){
    // if player's health goes to zero, then set state to GAMEOVER
    if (hero.health == 0 && states.PLAYING) {
            this.bga.pause();
            this.goa.play();
            state = states.GAMEOVER;
        }
        
    //s    console.log(hero.airborne);
   //     console.log(hero.level.x);
    
    if (hero.getPlayerX()>6600 && allBadGuysDefeated()) {
        this.bga.pause();
        this.wa.play();
        state = states.WON;
    }
    handleAI();
    level.update();
    hero.update();
    healthBar.setHealth(hero.health);
    
    // if hero falls into a hole
    if (hero.y > canvas.height - hero.spriteSize * 2 / 3) {
        hero.health = 0;
    }
}

function handleAI(){
    for (var n = 0; n < badGuys.length; n++) {
        var thisBadGuy = badGuys[n];
        
		// check if this bad guy can attack hero
        if (thisBadGuy.alive && hero.alive && !thisBadGuy.attacking && thisBadGuy.canAttack && thisBadGuy.nearby(hero)) {
            thisBadGuy.attack();
            setTimeout(function(){
                this.punch.load();
                this.punch.play();
                hero.damage();
            }, Constants.attackInterval);
        }
        thisBadGuy.update();
        
		// control bad guy movement
        if (thisBadGuy.alive) {
            if (thisBadGuy.isFacingRight()) {
                thisBadGuy.x += 5;
                if (!thisBadGuy.getZone().inBounds) {
                    thisBadGuy.facingRight = false;
                }
                thisBadGuy.x -= 5;
            }
            
            else {
                thisBadGuy.x -= 5;
                if (!thisBadGuy.getZone().inBounds) {
                    thisBadGuy.facingRight = true;
                }
                thisBadGuy.x += 5;
            }
        }
    }
}

function drawScreen(screenImg){
    context.drawImage(screenImg, 0, 0, canvas.width, canvas.height);
}

function resetGame(){
    level = {};
    hero = {};
    badGuys=[];
    initLevel();
    initHero();
    initBadGuys();
    initHealthBar();
    myAnimation.setTime(0);
    inittimer();

    this.wa.pause();
    this.wa.load();
    this.goa.pause();
    this.goa.load();
    this.bga.load();
    this.bga.play();
   
}
function allBadGuysDefeated(){
    for (var n = 0; n < badGuys.length; n++) {
        if (badGuys[n].alive) {
            return false;
        }
    }
    return true;
}
