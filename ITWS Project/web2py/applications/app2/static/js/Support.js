function Animation(canvas){
    // Stage vars    this.canvas = canvas;    this.context = canvas.getContext("2d");
    this.updateStage = undefined;
    this.drawStage = undefined;
    this.t = 0;    this.timeInterval = 1000 / 30;    this.intervalId = null;    this.frame = 0;};

Animation.prototype.setDrawStage = function(func){    this.drawStage = func;};
Animation.prototype.drawStage = function(){        this.drawStage();                   };
Animation.prototype.setUpdateStage = function(func){    this.updateStage = func;};
Animation.prototype.getContext = function(){    return this.context;};
Animation.prototype.getFrame = function(){    return this.frame;};
Animation.prototype.start = function(){
    var that = this;    this.drawStage();
    this.intervalId =  
         setInterval(function(){that.animationLoop();}, that.timeInterval);};
Animation.prototype.stop = function(){clearInterval(this.intervalId);};
Animation.prototype.getTimeInterval = function(){    return this.timeInterval;};
Animation.prototype.getTime = function(){
    return this.t;
};

Animation.prototype.setTime = function(time){
     this.t = time;
};

Animation.prototype.animationLoop = function(){
    this.frame++;
    this.t += this.timeInterval;
    this.updateStage();    this.drawStage();
};

function Level(config){
    this.x = config.x;
    this.y = config.y;
    this.leftBounds = config.leftBounds;
    this.rightBounds = config.rightBounds;
}

Level.prototype.draw = function(){
    context.save();
    context.drawImage(images.background, 0, 0);
    context.drawImage(images.level, this.x, this.y);
    context.restore();
};

Level.prototype.update = function(){
    // adjust level position
    if (hero.isFacingRight() && hero.x > this.rightBounds) { // Drawing image behind so as to look forward
        this.x -= (hero.x - this.rightBounds);
        hero.x = this.rightBounds;
    }
    else if (!hero.isFacingRight() && hero.x < this.leftBounds && this.x < -5) {
        this.x += (this.leftBounds - hero.x);
        hero.x = this.leftBounds;
    }
};

function HealthBar(config){
    this.maxHealth = config.maxHealth;
    this.x = config.x;
    this.y = config.y;
    this.maxWidth = config.maxWidth;
    this.height = config.height;
    this.health = this.maxHealth;
}

HealthBar.prototype.setHealth = function(health){
    this.health = health;
};

HealthBar.prototype.draw = function(){
    context.beginPath();
    context.save();
    var width = this.maxWidth * (this.health / this.maxHealth);
//    console.log(width);
    context.rect(this.x,this.y,this.maxWidth,this.height);
    context.fillStyle = "black";
    context.fill();
    context.closePath();
    context.beginPath();
    context.rect(this.x, this.y, width, this.height);
    context.fillStyle = "green";
    if(width<(this.maxWidth))
    {context.fillStyle = "orange";}
    if(width<=(2/5)*this.maxWidth)
    {context.fillStyle = "red";}
    context.fill(); 
    context.restore();
    context.closePath();
};

function timer(config){
    this.x = config.x;
    this.y = config.y;
    this.maxWidth = config.maxWidth;
    this.height = config.height;
    this.t = config.t;
    this.canvas = config.canvas;
}

timer.prototype.settime = function(t){
    this.t = t;
};

timer.prototype.returntime = function(t){
    return (Math.round(this.t)); 
};

timer.prototype.draw = function(t){
    var context = this.canvas.getContext("2d");
    context.beginPath();
    context.save();
    context.fillStyle = "black";
    context.fillRect(this.canvas.width - 200, 0, 200, 30);
    context.font = "18pt Calibri";
    context.fillStyle = "white"; 
//console.log(Math.round(t));
    context.fillText("Time(in s) " +t/1000 ,this.canvas.width - 180, 22);
    context.rect(this.x, this.y,this.maxWidth,this.height);
    context.restore();
    context.closePath();
};